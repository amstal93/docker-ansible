FROM python:3.9.5-slim-buster

WORKDIR /app
RUN apt-get update \
    && apt-get install -y --no-install-suggests --no-install-recommends \
        python3-venv \
        git \
        gosu \
        openssh-client \
        rsync \
        sshpass \
    && python3 -m venv env \
    && apt-get autoremove --purge -y python3-venv \
    && apt-get clean \
    && apt-get autoclean \
    && rm -rf /var/cache/apt/archives/* \
    && rm -rf /var/lib/apt/lists/*

COPY . .
RUN addgroup \
        --gid 5000 \
        ansible \
    && adduser \
        --gid 5000 \
        --uid 5000 \
        --home /var/lib/ansible \
        --shell /bin/bash \
        --gecos "Ansible IT Automation" \
        --disabled-password \
        ansible \
    && chown -R ansible:ansible . \
    && chmod 0700 . \
    && chmod 0600 ansible.cfg \
    && GOSU="gosu ansible:ansible" \
    && $GOSU env/bin/pip install --upgrade pip setuptools \
    && $GOSU env/bin/pip install -r requirements.txt \
    && rm -rf $HOME/.cache/pip

ENV PATH /app/env/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
CMD [ "gosu", "ansible:ansible", "ansible-playbook", "playbook.yml" ]